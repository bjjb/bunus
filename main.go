package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/go-redis/redis/v8"
	"github.com/spf13/cobra"
)

const name = "bunús"
const version = "0.0.1"

var addr string = ":8080"
var redisURL string = "redis://[::1]:6379"

type ctx struct {
	in       io.Reader
	out, err io.Writer
	exit     func(int)
	args     []string
}

var execute = func(os *ctx) {
	cmd := &cobra.Command{
		Use:     name,
		Version: version,
		Short:   "A URL fetching service",
		Long: `
A server which handles requests for particular UUIDs by matching them to a URL
which it then attempts to retrieve and copy to the response.  URLs can be
http, https, file or ssh; in the latter case, the server must have access.`,
		Run: func(c *cobra.Command, args []string) {
			o, err := redis.ParseURL(redisURL)
			if err != nil {
				fmt.Fprintln(c.ErrOrStderr(), err)
				os.exit(1)
			}
			r := redis.NewClient(o)
			if err := r.Ping(context.Background()).Err(); err != nil {
				fmt.Fprintln(c.ErrOrStderr(), err)
				os.exit(2)
			}
			fmt.Fprintf(os.out, "%s v%s (%s) listening on %s\n", name, version, redisURL, addr)
			http.ListenAndServe(addr, &handler{redis.NewClient(o), os})
		},
	}
	cmd.SetIn(os.in)
	cmd.SetOut(os.out)
	cmd.SetErr(os.err)
	cmd.SetArgs(os.args)

	cmd.PersistentFlags().StringVarP(&redisURL, "redis", "r", getEnv("REDIS_URL", redisURL), "redis URL")
	cmd.PersistentFlags().StringVarP(&addr, "addr", "a", getEnv("ADDR", addr), "listen address")

	cmd.Execute()
}

func main() {
	execute(&ctx{os.Stdin, os.Stdout, os.Stderr, os.Exit, os.Args[1:]})
}

type handler struct {
	r  *redis.Client
	os *ctx
}

func getEnv(name, fallback string) string {
	if v, found := os.LookupEnv(name); found {
		return v
	}
	return fallback
}

func httpError(w http.ResponseWriter, out io.Writer, status int, err error) {
	fmt.Fprintln(out, err)
	http.Error(w, http.StatusText(status), status)
}

func copyResponse(w http.ResponseWriter, r *http.Response) (int64, error) {
	defer r.Body.Close()
	for k, vv := range r.Header {
		for _, v := range vv {
			w.Header().Add(k, v)
		}
	}
	return io.Copy(w, r.Body)
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		if len(r.URL.Path) > 1 {
			id := r.URL.Path[1:]
			ctx := context.Background()
			if h.r.Exists(ctx, id).Val() == 1 {
				s := h.r.Get(ctx, id)
				if err := s.Err(); err != nil {
					httpError(w, h.os.err, http.StatusInternalServerError, err)
					return
				}
				u, err := url.Parse(s.Val())
				if err != nil {
					httpError(w, h.os.err, http.StatusInternalServerError, err)
					return
				}
				resp, err := http.Get(u.String())
				if err != nil {
					httpError(w, h.os.err, http.StatusInternalServerError, err)
					return
				}
				n, err := copyResponse(w, resp)
				if err != nil {
					httpError(w, h.os.err, http.StatusInternalServerError, err)
					return
				}
				fmt.Fprintf(h.os.out, "copied %d bytes from %s to %s for %s\n", n, u.String(), r.RemoteAddr, id)
				return
			}
		}
		http.NotFound(w, r)
		return
	}
	http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
}
