module gitlab.com/bjjb/bunús

go 1.14

require (
	github.com/go-redis/redis/v8 v8.4.8
	github.com/spf13/cobra v1.1.1
)
