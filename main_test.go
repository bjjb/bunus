package main

import (
	"bytes"
	"os"
	"testing"
)

func Test_main(t *testing.T) {
	defer func(e func(*ctx)) { execute = e }(execute)

	var got *ctx
	execute = func(ctx *ctx) { got = ctx }

	main()

	if got.in != os.Stdin {
		t.Errorf("expected %v, got %v", os.Stdin, got.in)
	}
	if got.out != os.Stdout {
		t.Errorf("expected %v, got %v", os.Stdout, got.out)
	}
	if got.err != os.Stderr {
		t.Errorf("expected %v, got %v", os.Stderr, got.err)
	}
	if got.exit == nil {
		t.Errorf("expected an exit function")
	}
	for i, a := range os.Args[1:] {
		if got.args[i] != a {
			t.Errorf("expected args[%d] to be %q, got %q", i, a, got.args[i])
		}
	}
}

func Test_execute(t *testing.T) {
	out, err := new(bytes.Buffer), new(bytes.Buffer)
	execute(&ctx{nil, out, err, nil, []string{"foo"}})
}
