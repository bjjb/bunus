Bunús
=====

Serves URLs from here.

This package provides a http.Server which can match a UUID to a URL, and then
copy the contents at that URL to the response.
